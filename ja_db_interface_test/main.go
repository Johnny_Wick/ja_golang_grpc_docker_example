package main

import (
	"fmt"
)


type resource_db_interface interface{
	query_by_id( int ) string   //return value shall be map like
	query_by_str( string) string  // return value shall be map like
}

type mongodb_inst struct{
	db_instance string // implemnt the real strucutre later
	db_collection string 
}

type postgres_inst struct{
	db_instance string // implemnt the real strucutre later
	db_table string
}

func (my_mongo mongodb_inst) query_by_id( query_user_id int) string {
	fmt.Println("[Info]: using mongodb.query_by_id, id is ",query_user_id)
	fmt.Println("[Info]: Current apply collection is ",my_mongo.db_collection)
	fmt.Println("[Info]: Current apply db_instance is ",my_mongo.db_instance)
	fmt.Println("[Info]: Fake the return data for now")
	return "fake_data_from_mongo"
}

func (my_mongo mongodb_inst) query_by_str( query_str string) string{
	fmt.Println("[Info]: using mongodb.query_by_id, id is ",query_str)
	fmt.Println("[Info]: Current apply collection is ",my_mongo.db_collection)
	fmt.Println("[Info]: Current apply db_instance is ",my_mongo.db_instance)
	fmt.Println("[Info]: Fake the return data for now")
	return "fake_data_from_mongo"

}

func (my_postgres postgres_inst) query_by_id( query_user_id int) string {
	fmt.Println("[Info]: using postgres.query_by_id, id is ",query_user_id)
	fmt.Println("[Info]: Current apply table is ",my_postgres.db_table)
	fmt.Println("[Info]: Current apply db_instance is ",my_postgres.db_instance)
	fmt.Println("[Info]: Fake the return data for now")
	return "fake_data_from_mongo"
}

func (my_postgres postgres_inst) query_by_str( query_str string) string {
	fmt.Println("[Info]: using postgres.query_by_id, id is ",query_str)
	fmt.Println("[Info]: Current apply table is ",my_postgres.db_table)
	fmt.Println("[Info]: Current apply db_instance is ",my_postgres.db_instance)
	fmt.Println("[Info]: Fake the return data for now")
	return "fake_data_from_mongo"

}


func checking_interface(my_db_inter resource_db_interface){
	fmt.Println("-------------------------------------------")
	fmt.Println(my_db_inter)
	fmt.Println(my_db_inter.query_by_id(22))
	fmt.Println(my_db_inter.query_by_str("query_jason"))
	fmt.Println("-------------------------------------------")
}


func main(){

	my_mongo := mongodb_inst{db_instance: "mongodb_092",db_collection:"user_collection"}
	my_posgres := postgres_inst{db_instance: "posgres_002",db_table:"game_table"}

	checking_interface(my_mongo)
	checking_interface(my_posgres)



}
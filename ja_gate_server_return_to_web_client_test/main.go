package main

import (
   "encoding/json"
   "fmt"
   "github.com/google/uuid"
   "gopkg.in/oauth2.v3/models"
   "log"
   "net/http"
   //"time"

   "gopkg.in/oauth2.v3/errors"
   "gopkg.in/oauth2.v3/manage"
   "gopkg.in/oauth2.v3/server"
   "gopkg.in/oauth2.v3/store"
   "github.com/sirupsen/logrus"
   "os/exec"
)

const norm_de_server = "ja_gate_server_v1"

func main() {

	logrus.Info(fmt.Sprintf("[%v] Checking Existed Port...",norm_de_server))
	cmd_out, err := exec.Command("./ja_check_listenning_port").Output()
	if err != nil{
		fmt.Println(err)
	}
	
	fmt.Printf("\n\n%s\n\n",cmd_out)

	var server_port int
	logrus.Info("Please Enter your Server port setting.  e.g : 9527")
	_ , err  = fmt.Scan(&server_port)
	if err != nil{
		logrus.Error("Fail to set the port numbe: ",server_port,err)
	}

	var store_port int
	logrus.Info("Please Enter your Client Store port setting.  e.g : 9094")
	_ , err  =fmt.Scan(&store_port)
	if err != nil{
		logrus.Error("Fail to set the port numbe: ",store_port,err)
	}


	logrus.Info("server_port is  ", server_port)
	logrus.Info("store_port  is  ", store_port)
    
 

	// Main Cod3 
    manager := manage.NewDefaultManager()
    manager.SetAuthorizeCodeTokenCfg(manage.DefaultAuthorizeCodeTokenCfg)
 
    // token memory store
    manager.MustTokenStorage(store.NewMemoryTokenStore())
 
    // client memory store
    clientStore := store.NewClientStore()
    
    manager.MapClientStorage(clientStore)
 
    srv := server.NewDefaultServer(manager)
    srv.SetAllowGetAccessRequest(true)
    srv.SetClientInfoHandler(server.ClientFormHandler)
    manager.SetRefreshTokenCfg(manage.DefaultRefreshTokenCfg)
 
    srv.SetInternalErrorHandler(func(err error) (re *errors.Response) {
       log.Println("Internal Error:", err.Error())
       return
    })
 
    srv.SetResponseErrorHandler(func(re *errors.Response) {
       log.Println("Response Error:", re.Error.Error())
    })
 
    http.HandleFunc("/token", func(w http.ResponseWriter, r *http.Request) {
		logrus.Info(fmt.Sprintf("[%v]: Get Request at /token endpoint",norm_de_server))
        srv.HandleTokenRequest(w, r)
    })
 
    http.HandleFunc("/credentials", func(w http.ResponseWriter, r *http.Request) {

		//logrus.Info("Get Request at /credentials endpoint")
		logrus.Info(fmt.Sprintf("[%v]: Get Request at /token endpoint ",norm_de_server))

        clientId := uuid.New().String()[:8]
        clientSecret := uuid.New().String()[:8]
		logrus.Debug(fmt.Sprintf("clientId:                %v",clientId))
		logrus.Debug(fmt.Sprintf("clientSecret:            %v",clientSecret))


        err := clientStore.Set(clientId, &models.Client{
           ID:     clientId,
           Secret: clientSecret,
           Domain: "http://localhost:9094", //implement
        })
        if err != nil {
           fmt.Println(err.Error())
        }
  
        w.Header().Set("Content-Type", "application/json")
        json.NewEncoder(w).Encode(map[string]string{"CLIENT_ID": clientId, "CLIENT_SECRET": clientSecret})
    })
 
    http.HandleFunc("/protected", validateToken(func(w http.ResponseWriter, r *http.Request) {

		logrus.Info(fmt.Sprintf("[%v]: Get Request at /protected endpoint",norm_de_server))
		//logrus.Debug(fmt.Sprintf("     The request is\n%v\n",http.Request))

		// Response to client
        w.Write([]byte("Hello, I'm protected"))
		//logrus.Debug(fmt.Sprintf("     The response is\n%v\n",http.ResponseWriter))

    }, srv))
 
 
 


	// Server message
	credentials_url := fmt.Sprintf("http://localhost:%v/credentials",server_port)
	// http://localhost:9096/token?grant_type=client_credentials&client_id=YOUR_CLIENT_ID&client_secret=YOUR_CLIENT_SECRET&scope=all	
	token_url       := fmt.Sprintf("http://localhost:%v/token?grant_type=client_credentials&client_id=YOUR_CLIENT_ID&client_secret=YOUR_CLIENT_SECRET&scope=all",server_port)
	protected_url      := fmt.Sprintf("http://localhost:%v/protected?access_token=YOUR_TOKEN",server_port)

	logrus.Info(fmt.Sprintf("[%v]: %v",norm_de_server,credentials_url)) 
	logrus.Info(fmt.Sprintf("[%v]: %v",norm_de_server,token_url)) 
	logrus.Info(fmt.Sprintf("[%v]: %v",norm_de_server,protected_url)) 

	
	// ListenAndServe here:  
		// log.Fatal(http.ListenAndServe(":9096",nil))
	input_port := fmt.Sprintf(":%v",server_port)
	log.Fatal(http.ListenAndServe(input_port,nil))

}

func validateToken(f http.HandlerFunc, srv *server.Server) http.HandlerFunc {
   return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
      _, err := srv.ValidationBearerToken(r)
      if err != nil {
         http.Error(w, err.Error(), http.StatusBadRequest)
         return
      }

      f.ServeHTTP(w, r)
   })
}
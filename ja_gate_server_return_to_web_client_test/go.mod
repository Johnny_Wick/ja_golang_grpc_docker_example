module ja_gate_server_return_to_web_client_test

go 1.12

require (
	github.com/google/uuid v1.1.1 // indirect
	github.com/sirupsen/logrus v1.4.2 // indirect
	gopkg.in/oauth2.v3 v3.12.0 // indirect
)

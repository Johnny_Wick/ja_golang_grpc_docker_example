## Result
owner: Jason
---

- run the server


- access enpoint credential to make register
---
![result](img/credentials.jpg)

---

- get the token by sending client_id and client_secret(password) 

---
![result](img/get_token.jpg)
---

- go to enpoint /protected to test with or without token 
---
- valid token 
---
![result](img/token_pass.jpg)
--- 
- invalid token
---
![result](img/invalid_token.jpg) 
---





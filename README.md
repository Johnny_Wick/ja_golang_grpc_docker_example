##  Golang Docker , GRPC Example
Writer : Jason.  🦜....
---
- Avec go mod

## Nail Items Shop Code (not implement yet)
---















## ja_jwt_token_test project 😈
---
- test the jwt token with future auth and gate server base function test ✅
---
- files:
    - ja_api_test.sh (automation) ✅
        - auto post /login avec curl cmd (body data: username, password)
        - auto get /resource avec curl cmd  (header , authorization: barrer jwt_token_str)

ja_api_test result
---
![result](img/ja_api_test.jpg)

---


##  About JWT Middleware Vdlidation Function
---
- use request.ParseFromRequest(request, extractor, return_keyfunction )
- then thckeing the token.Valid
----

See Code Snapshot
---
![Img](img/middle_ware_token_valid.jpg)

---




## ja_gate_server_v1   🐸

---
- grpc with restapi 
- /credential to get the user_name, user_secret
- /token, to get the token
- /protected, with token to get into the url
- __it's using the 3rd pckage, I personally don't want to use that__ 😈..

---





---




---
## Note:
---

- Proto Content:
    - repated , indicate the multi-data-structure
    - Cap. with message  (Container)
    - lower_case with variavble_instance (containers)
    - e.g.
        - repeated Container containers = 5;

---
![proto_ref](img/proto_ref.jpg)
---

- skip generated pb.go part with protoc command
- check yourself ! 😈

- server.go
    - use refelection package to register the new type of Server Instance
    - check the error , when serving is ok or not
    - log.Fatalf or use logrus.Error() , both are ok
    - custom logger proxy code without any depency in the following service code is another good way to implement later 😓 

  

---  

![server](img/server.jpg) 
--- 

## Json format to GRPC Request Format
---
- JSON to GRPC Requesst format 
![json_to_grpc_req](img/json_to_grpc_request.jpg)

---

Client with Deffer function

---

![client](img/client.jpg)

---


## Port management with different Service in different port
---

![Check](img/port_management.jpg)

---





---

### Referenced Link:
- https://dzone.com/articles/create-versatile-microservices-in-golang-part-1 (Author: Ewan Valentine)
- and so on

package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/codegangsta/negroni"
	"github.com/dgrijalva/jwt-go"
	"github.com/dgrijalva/jwt-go/request"
	"github.com/sirupsen/logrus"
)

const (
	SecretKey = "ja_jwt_gate"
)


// Save Time to check err all the time
func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

type UserCredentials struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type User struct {
	ID       int    `json:"id"`
	Name     string `json:"name"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type Response struct {
	Data string `json:"data"`
}

type Token struct {
	Token string `json:"token"`
}

func StartServer() {

	http.HandleFunc("/login", LoginHandler)

	http.Handle("/resource", negroni.New(
		negroni.HandlerFunc(ValidateTokenMiddleware),
		negroni.Wrap(http.HandlerFunc(ProtectedHandler)),
	))

	logrus.Info("Now listening...")
	logrus.Warn("Now listening...")
	logrus.Debug("Now listening...")


	log.Println("\nPOST:  http://localhost:8080/login  with body")
	log.Println("       { \"username\": \"johnny\" " )
	log.Println("        \"password\": \"P@sswordXXXX\" } ")
	log.Println("-------------------------------------\n")
	log.Println(" GET    http://localhost:8080/resource with head")
	log.Println("       authorization:     YOUR_TOKEN_STRING ")
	log.Println("--------------------------------------------")

	http.ListenAndServe(":8080", nil)

	

}

func main() {
	StartServer()
}

func ProtectedHandler(w http.ResponseWriter, r *http.Request) {

	response := Response{"Gained access to protected resource"}
	JsonResponse(response, w)

}

func LoginHandler(w http.ResponseWriter, r *http.Request) {

	var user UserCredentials

	err := json.NewDecoder(r.Body).Decode(&user)

	if err != nil {
		w.WriteHeader(http.StatusForbidden)
		fmt.Fprint(w, "Error in request")
		return
	}

	// Implement MongoDB Here
	if strings.ToLower(user.Username) != "johnny" {
		if user.Password != "P@sswordXXXX" {
			w.WriteHeader(http.StatusForbidden)
			fmt.Println("Error logging in")
			fmt.Fprint(w, "Invalid credentials")
			return
		}
	}

	// Creer TOKEN
	// Declare Sign Method avec HS256
	token := jwt.New(jwt.SigningMethodHS256)
	claims := make(jwt.MapClaims)

	// Expire Time Setting
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(1)).Unix()

	// Issue At time Setting
	claims["iat"] = time.Now().Unix()
	token.Claims = claims

	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintln(w, "Error extracting the key")
		fatal(err)
	}

	// Signed by the ScretKey
		// var jwtKey = []byte("my_secret_key")
	    // tokenString, err := token.SignedString(jwtKey)
	// Johnny: Try code , b/a, signing process of tokenString and token
	logrus.Info("Before Signing...")
	fmt.Printf("%+v",token)

	tokenString, err := token.SignedString([]byte(SecretKey))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintln(w, "Error while signing the token")
		fatal(err)
	}

	logrus.Info("\nAfter Signing...\n")
	fmt.Printf("%+v",token)

	// Debug
	logrus.Debug("The Token Str is \n    ", tokenString,"\n")


	response := Token{tokenString}
	JsonResponse(response, w)

}

func ValidateTokenMiddleware(w http.ResponseWriter, r *http.Request, next http.HandlerFunc) {

	//Check the token is valid or not
	token, err := request.ParseFromRequest(r, request.AuthorizationHeaderExtractor,
		func(token *jwt.Token) (interface{}, error) {
			fmt.Println("Inside key func:")
			fmt.Println("--------------")
			claims := token.Claims.(jwt.MapClaims)
		    fmt.Printf("Token for user %v expires %v", claims["user"], claims["exp"])
			fmt.Printf("%+v",token)
			fmt.Println("--------------")
			return []byte(SecretKey), nil
		})

	fmt.Println("Outside key func: inside Middleware:")
	fmt.Println("--------------")
	fmt.Printf("%+v",token)
	claims := token.Claims.(jwt.MapClaims)
	fmt.Printf("Token for user %v expires %v", claims["user"], claims["exp"])
	fmt.Println("--------------")


	if err == nil {
		if token.Valid {
			next(w, r)
		} else {
			w.WriteHeader(http.StatusUnauthorized)
			fmt.Fprint(w, "Token is not valid")
		}
	} else {
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprint(w, "Unauthorized access to this resource")
	}

}

func JsonResponse(response interface{}, w http.ResponseWriter) {

	json, err := json.Marshal(response)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	w.Write(json)
}



/*
Origin From：https://blog.csdn.net/wangshubo1989/article/details/74529333
*/
module ja_jwt_token_test

go 1.12

require (
	github.com/codegangsta/negroni v1.0.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/sirupsen/logrus v1.4.2 // indirect
)

!#/bin/bash

run_get(){
    echo $1
    echo $2
    echo $3
    echo $4
    # curl -d @get_request.json http://localhost:8080/login
    echo "----"
    $1 $2 $3 $4
    echo "----"
}

# [Jean]: !!!
    # Tu peut implement -h Header , ca apres
        # curl -d @get_request.json  -H "Content-Type: application/json" @ http://localhost:8080/login


echo "[Jean]: Use Get pour toi avec get_request.json"
echo "Par Example: curl -d @get_request.json http://localhost:8080/login"
echo "------------------------------"
read curl_cmd pa_cmd json_cmd url_cmd

run_get $curl_cmd $pa_cmd $json_cmd $url_cmd


# curl -d @get_request.json http://localhost:8080/login


echo
echo
echo "[Jean]: Use the token string in the screen"
echo "-------------------------------------------"
echo
read my_token_str
echo "-----------------"
echo $my_token_str
echo "-----------------"
echo
echo "[Jean]: Ensuite type the resurce endpoint"
echo "Par Example: curl -H \"Accept: application/json\" -H \"Authorization: Bearer \${TOKEN}\" http://localhost:8080/resource"
echo "type resource_url"
echo
read resource_url 

#curl -H "Accept: application/json" -H "Authorization: Bearer ${TOKEN}" http://localhost:8080/resource
curl -H "Accept: application/json" -H "Authorization: Bearer ${my_token_str}" $resource_url
echo 
echo "############## The end ########################################"
echo 


